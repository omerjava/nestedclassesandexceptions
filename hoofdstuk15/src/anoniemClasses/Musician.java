package anoniemClasses;

/*
Lokaal Geneste klasse:

Maak van de Instrument lokaal geneste klasse en
maak een instantie van een Instrument in de play() methode
 om te bespelen.
Maak nadien een Musician in de main() methode en laat die spelen
___
Anoniem Geneste klasse:
Ga de klasse Instrument gaan afzonderen als een aparte interface. Verander de aanmaak van Instrument
in Musician klasse (play() methode), zodanig dat die het Instrument aanmaakt aan de hand van
een anoniem geneste klasse.
Hierbij is het de bedoeling makeSound() methode abstract is.
 */

public class Musician {

    void play(){
        Instrument instrument = new Instrument(){
            @Override
            public void makeSound() {
                System.out.println("Anoniem class is used with interface..hurrayyy :)");
            };
        };

        instrument.makeSound();
    }
}
