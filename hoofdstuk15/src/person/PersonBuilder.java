package person;

public class PersonBuilder {

    private String firstname;
    private String lastname;
    private String gender;
    private String telNumber;
    private String mail;
    private String address;
    private String nationalNumber;

    public PersonBuilder withFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

   public PersonBuilder withLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public PersonBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder withTelNumber(String telNumber) {
        this.telNumber = telNumber;
       return this;
    }
    public PersonBuilder withMail(String mail) {
        this.mail = mail;
       return this;
    }
    public PersonBuilder withAddress(String address) {
        this.address = address;
        return this;
    }
  public PersonBuilder withNationalNumber(String nationalNumber) {
        this.nationalNumber = nationalNumber;
        return this;
    }



}
