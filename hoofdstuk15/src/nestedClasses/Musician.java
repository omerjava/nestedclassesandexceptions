package nestedClasses;

public class Musician {


    void play(){
        Instrument instrument = new Instrument();
        instrument.makeSound();
    }

    static class Instrument {

        void makeSound(){
            System.out.println("La la laa laa..Do do doo...Mi mi mii..");
        }
    }
}
