package nestedClasses;

/*
    Maak een klasse voor huisdier:

naam
Addres
Eiegenaar
ChipNummer
Stamboom (boolean)
Soort

Maak een statisch geneste klasse om een dier op te maken aan de hand van een builder
     */

public class Huisdier {
    private String name;
    private String address;
    private String eigenaar;
    private int chipnummer;
    private boolean stamboon;
    private String type;

    public Huisdier(){

    }

    public Huisdier(String name, String address, String eigenaar, int chipnummer, boolean stamboon, String type) {
       setName(name);
       setAddress(address);
       setEigenaar(eigenaar);
       setChipnummer(chipnummer);
       setStamboon(stamboon);
       setType(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEigenaar() {
        return eigenaar;
    }

    public void setEigenaar(String eigenaar) {
        this.eigenaar = eigenaar;
    }

    public int getChipnummer() {
        return chipnummer;
    }

    public void setChipnummer(int chipnummer) {
        this.chipnummer = chipnummer;
    }

    public boolean isStamboon() {
        return stamboon;
    }

    public void setStamboon(boolean stamboon) {
        this.stamboon = stamboon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class HuisdierBuilder {
        private String name;
        private String address;
        private String eigenaar;
        private int chipnummer;
        private boolean stamboon;
        private String type;

        public HuisdierBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public HuisdierBuilder withAddress(String address) {
            this.address = address;
            return this;
        }

        public HuisdierBuilder withEigenaar(String eigenaar) {
            this.eigenaar = eigenaar;
            return this;
        }

        public HuisdierBuilder withChipnummer(int chipnummer) {
            this.chipnummer = chipnummer;
            return this;
        }

        public HuisdierBuilder withStamboon(boolean stamboon) {
            this.stamboon = stamboon;
            return this;
        }

        public HuisdierBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public Huisdier build() {
            if (name != null || address != null || eigenaar != null) {
                return new Huisdier(name, address, eigenaar, chipnummer, stamboon, type);
            }
            return null;
        }


    }

    @Override
    public String toString() {
        return "Huisdier{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", eigenaar='" + eigenaar + '\'' +
                ", chipnummer=" + chipnummer +
                ", stamboon=" + stamboon +
                ", type='" + type + '\'' +
                '}';
    }
}
