package nestedClasses;

public class HuisdierApp {

    public static void main(String[] args) {
        Huisdier dier1 = new Huisdier("Terry","Steenweg 50, Brussels", "John Doe", 43342, true, "bird");

        System.out.println("Dier1: " + dier1);

        Huisdier dier2 = new Huisdier.HuisdierBuilder().withName("Pamuk").withAddress("Palaceweg, Mechelen")
                .withEigenaar("Kate").withChipnummer(23432).withStamboon(true).withType("cat").build();

        System.out.println("Dier2: " + dier2);

        Huisdier dier3 = new Huisdier.HuisdierBuilder().withName("Kitty").withAddress("Kattenberg, Mechelen")
                .withEigenaar("James").build();

        System.out.println("Dier3: " + dier3);

    }
}
