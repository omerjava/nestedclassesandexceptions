package nestedClasses;

public class MusicianApp {

    public static void main(String[] args) {
        System.out.println("musician object in main app is working");
        Musician musician = new Musician();
        musician.play();

        System.out.println("instrument object in main app is working");
        Musician.Instrument instrument = new Musician.Instrument();
        instrument.makeSound();
    }
}
